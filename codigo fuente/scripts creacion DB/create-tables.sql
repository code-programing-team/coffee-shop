create database coffeeShop_Db;
use coffeeShop_Db;
create table usuario(
codigo_usu int auto_increment not null,
nombre_usu varchar(100) not null,
correo_usu varchar(100) not null,
pws_usu varchar(10) not null,
constraint usuario_pk primary key(codigo_usu)
);

create table pedido(
codigo_pedido int auto_increment not null,
codigo_usu int not null,
constraint pedido_pk primary key(codigo_pedido),
constraint pedido_codigo_usu_fk foreign key(codigo_usu) references usuario(codigo_usu)
);

create table producto(
codigo_product int auto_increment not null, 
nombre_product varchar(100) not null,
tipo_product varchar(100) not null,
img_product BLOB, 
descripcion_product varchar(500) not null, 
tiempo_prep int not null, 
costo_product double not null,
constraint producto_pk primary key(codigo_product)
);

create table detail_pedido(
codigo_pedido int not null, 
codigo_product int not null,
numero_mesa int not null,
fecha_pedido date not null,
cantidad_product int not null,
costo_total_pedido double not null,
constraint detail_pedido_pk primary key(codigo_pedido,codigo_product),
constraint detail_pedido_codigo_pedido_fk foreign key(codigo_pedido) references pedido(codigo_pedido),
constraint detail_pedido_codigo_product_fk foreign key(codigo_product) references producto(codigo_product)
);

create table venta(
codigo_venta int auto_increment not null, 
codigo_pedido int not null, 
fecha_venta date not null, 
costo_total_venta double not null,
constraint venta_pk primary key(codigo_venta),
constraint venta_codigo_pedido_fk foreign key(codigo_pedido) references pedido(codigo_pedido)
);

create table factura(
codigo_factura int auto_increment not null,
codigo_venta int not null, 
fecha_factura date not null,
constraint factura_pk primary key(codigo_factura),
constraint factura_codigo_venta_fk foreign key(codigo_venta) references venta(codigo_venta)
);

